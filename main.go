package main

import (
	"crypto/md5"
	"encoding/hex"
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"

	"github.com/pkg/errors"
)

func main() {
	source := flag.String("source", "", "source directory which files needs to be in target")
	target := flag.String("target", "", "target directory where the source files needs to be found")
	//	sourceCacheFilename := flag.String("source-cache-filename", "", "cache file for source data")
	//	targetCacheFilename := flag.String("target-cache-filename", "", "cache file for target data")

	flag.Parse()

	if source == nil || len(*source) <= 0 {
		fmt.Printf("ERROR: source argument required\n")
		os.Exit(1)
	}
	if stat, err := os.Stat(*source); os.IsNotExist(err) {
		fmt.Printf("ERROR: source not found\n")
		os.Exit(3)
	} else if err != nil {
		fmt.Printf("ERROR: unable to stat source: %s\n", err.Error())
		os.Exit(4)
	} else if !stat.IsDir() {
		fmt.Printf("ERROR: source is not a directory\n")
	}

	if target == nil || len(*target) <= 0 {
		fmt.Printf("ERROR: target argument required\n")
		os.Exit(2)
	}
	if stat, err := os.Stat(*target); os.IsNotExist(err) {
		fmt.Printf("ERROR: target not found\n")
		os.Exit(3)
	} else if err != nil {
		fmt.Printf("ERROR: unable to stat target: %s\n", err.Error())
		os.Exit(4)
	} else if !stat.IsDir() {
		fmt.Printf("ERROR: target is not a directory\n")
	}

	/*	if sourceCacheFilename == nil || len(*sourceCacheFilename) <= 0 {
			fmt.Printf("ERROR: source cache filename argument required\n")
			os.Exit(5)
		}

		if targetCacheFilename == nil || len(*targetCacheFilename) <= 0 {
			fmt.Printf("ERROR: target cache filename argument required\n")
			os.Exit(6)
		}*/

	sourceCache, err := NewCache(*source)
	if err != nil {
		fmt.Printf("ERROR: establishing source cache failed: %s\n", err.Error())
		os.Exit(9)
	}
	targetCache, err := NewCache(*target)
	if err != nil {
		fmt.Printf("ERROR: establishing target cache failed: %s\n", err.Error())
		os.Exit(10)
	}

	files, err := targetCache.Diff(sourceCache)
	if err != nil {
		fmt.Printf("ERROR unable to diff: %s\n", err.Error())
		os.Exit(11)
	}

	for _, file := range files {
		fmt.Printf("%s\n", file.Filename)
	}
}

// ---------------------- File -----------------------------

type File struct {
	Filename string    `json:"filename"`
	Checksum string    `json:"checksum"`
	Filesize int64     `json:"filesize"`
	Modified time.Time `json:"modified"`
}

func NewFile(filename string, info os.FileInfo) (*File, error) {
	if info.IsDir() {
		return nil, errors.New(fmt.Sprintf("Got dir, file expected: %s", filename))
	}

	f := File{
		Filename: filename,
		Filesize: info.Size(),
		Modified: info.ModTime(),
	}

	file, err := os.Open(f.Filename)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("unable to open: %s", filename))
	}
	defer file.Close()

	hash := md5.New()
	if _, err := io.Copy(hash, file); err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("unable to read file: %s", filename))
	}

	f.Checksum = hex.EncodeToString(hash.Sum(nil))

	return &f, nil
}

// ---------------------- File Cache -----------------------------

type Cache struct {
	Files           []*File        `json:"files"`
	FilesByFilename map[string]int `json:"file_by_filename"`
	FilesByChecksum map[string]int `json:"file_by_checksum"`
}

func NewCache(directory string) (*Cache, error) {
	cache := Cache{}
	cache.FilesByFilename = make(map[string]int)
	cache.FilesByChecksum = make(map[string]int)

	err := filepath.Walk(directory, func(filename string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		if err != nil {
			return errors.Wrap(err, fmt.Sprintf("unable to walk with %s", filename))
		}

		file := cache.GetByFilename(filename)
		if file == nil {
			file, err = NewFile(filename, info)
		} else {
			file = nil
		}
		if err != nil {
			return errors.Wrap(err, fmt.Sprintf("unable to create file: %s", filename))
		}

		if file == nil {
			// File was cached
			return nil
		}

		if err := cache.Append(file); err != nil {
			return errors.Wrap(err, "unable to append file to cache")
		}

		return nil
	})
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("unable to walk: %s", directory))
	}

	return &cache, nil
}

func (c *Cache) GetByFilename(filename string) *File {
	i, ok := c.FilesByFilename[filename]
	if ok {
		return c.Files[i]
	}
	return nil
}

func (c *Cache) GetByChecksum(checksum string) *File {
	i, ok := c.FilesByChecksum[checksum]
	if ok {
		return c.Files[i]
	}
	return nil
}

func (c *Cache) Append(file *File) error {
	c.FilesByFilename[file.Filename] = len(c.Files)
	c.FilesByChecksum[file.Checksum] = len(c.Files)
	c.Files = append(c.Files, file)

	return nil
}

func (c *Cache) Diff(other *Cache) ([]*File, error) {
	var ret []*File

	for _, file := range other.Files {
		if c.GetByChecksum(file.Checksum) == nil {
			ret = append(ret, file)
		}
	}

	return ret, nil
}
